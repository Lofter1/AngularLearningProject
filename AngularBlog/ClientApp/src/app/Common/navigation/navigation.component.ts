import { Component, OnInit } from '@angular/core';
import {UserService} from '../../user/user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  isCollapsed = true;

  constructor(private loginService: UserService) { }

  ngOnInit() {
  }

}
