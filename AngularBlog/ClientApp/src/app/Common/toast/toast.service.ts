import { Injectable } from '@angular/core';
import { Toast } from './toast';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  notifications: Toast[] = [];

  constructor() { }

  show(head: string, message: string, delay = 5000): void {
    this.notifications.push({head, message, delay});
  }

  remove(notification: Toast) {
    this.notifications = this.notifications.filter(note => note !== notification);
  }
}
