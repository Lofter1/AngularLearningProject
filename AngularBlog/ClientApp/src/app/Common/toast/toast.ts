export class Toast {
  head: string;
  message: string;
  delay: number;
}
