export class BlogPost {
  id: number;
  title: string;
  content: string;
  date: Date;
  estimatedReadTime: number;
}
