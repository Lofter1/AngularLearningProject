import { Injectable } from '@angular/core';
import {BlogPost} from '../Models/BlogPost';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlogPostRepositoryService {
  postUrl = 'http://localhost:49329/api/posts';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {}

  getAll(): Observable<BlogPost[]> {
    return this.http.get<BlogPost[]>(this.postUrl);
  }

  getPost(id: number): Observable<BlogPost> {
    const url = `${this.postUrl}/${id}`;
    return this.http.get<BlogPost>(url);
  }

  add(post: BlogPost): Observable<BlogPost> {
    return this.http.post<BlogPost>(this.postUrl, post, this.httpOptions);
  }
}
