import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../Models/User';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserRepositoryService {
  userUrl = 'http://localhost:49329/api/users';

  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl);
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(this.userUrl + `/${id}`).pipe(tap(u => console.log(u.id)));
  }

  add(newUser: User): Observable<User> {
    return this.http.post<User>(this.userUrl, newUser);
  }
}
