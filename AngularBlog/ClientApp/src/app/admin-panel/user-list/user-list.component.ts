import { Component, OnInit } from '@angular/core';
import {UserRepositoryService} from '../../Repositories/user-repository.service';
import {User} from '../../Models/User';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  private allUsers: User[];

  constructor(private users: UserRepositoryService) { }

  ngOnInit() {
    this.users.getAll().subscribe(users => this.allUsers = users);
  }

}
