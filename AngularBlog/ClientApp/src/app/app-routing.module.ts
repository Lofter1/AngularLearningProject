import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BlogComponent} from './blog/blog.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AddBlogComponent} from './blog/add-blog/add-blog.component';
import {BlogpostComponent} from './blog/blogpost/blogpost.component';
import {LoginComponent} from './user/login/login.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {OnlyAdminsGuardService} from './guards/only-admins-guard.service';
import {RegisterComponent} from './user/register/register.component';

const routes: Routes = [
  { path: 'blog', component: BlogComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'blog/NewEntry', component: AddBlogComponent, canActivate: [OnlyAdminsGuardService] },
  { path: 'blog/post/:id', component: BlogpostComponent },
  { path: 'login', component: LoginComponent},
  { path: 'admin', component: AdminPanelComponent, canActivate: [OnlyAdminsGuardService] },
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
