import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { BlogComponent } from './blog/blog.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddBlogComponent } from './blog/add-blog/add-blog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BlogEditorComponent } from './blog/blog-editor/blog-editor.component';
import { HttpClientModule} from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { BlogpostComponent } from './blog/blogpost/blogpost.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill';
import { ToastComponent } from './Common/toast/toast.component';
import { PostPreviewComponent } from './blog/blogpost/post-preview/post-preview.component';
import { LoginComponent } from './user/login/login.component';
import { NavigationComponent } from './Common/navigation/navigation.component';
import {NgxPermissionsModule, NgxPermissionsRestrictStubModule} from 'ngx-permissions';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { RegisterComponent } from './user/register/register.component';
import { UserListComponent } from './admin-panel/user-list/user-list.component';


@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    DashboardComponent,
    AddBlogComponent,
    BlogEditorComponent,
    BlogpostComponent,
    ToastComponent,
    PostPreviewComponent,
    LoginComponent,
    NavigationComponent,
    AdminPanelComponent,
    RegisterComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),

    NgbModule,
    QuillModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    NgxPermissionsRestrictStubModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
