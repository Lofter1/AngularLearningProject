import {Component, OnInit} from '@angular/core';
import {BlogPost} from '../../Models/BlogPost';
import {BlogPostRepositoryService} from '../../Repositories/blog-post-repository.service';
import {Router} from '@angular/router';
import {ToastService} from '../../Common/toast/toast.service';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {
  newBlogEntry: BlogPost;

  constructor(private blogPostRepository: BlogPostRepositoryService,
              private router: Router,
              private toastService: ToastService) { }

  ngOnInit() {
    this.newBlogEntry = new BlogPost();
    this.newBlogEntry.content = '';
  }

  submit(): void {
    this.blogPostRepository.add(this.newBlogEntry).subscribe();
    this.toastService.show('Added post', `Added toast ${this.newBlogEntry.title}`, 5000);
    this.router.navigateByUrl('blog');
  }
}
