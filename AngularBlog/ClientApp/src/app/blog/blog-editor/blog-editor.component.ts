import {Component, Input, OnInit} from '@angular/core';
import {BlogPost} from '../../Models/BlogPost';
import { ReadingTimeEstimatorService } from '../reading-time-estimator.service';
import * as htmlStripper from 'striptags';


@Component({
  selector: 'app-blog-editor',
  templateUrl: './blog-editor.component.html',
  styleUrls: ['./blog-editor.component.css']
})
export class BlogEditorComponent implements OnInit {
  @Input() BlogEntry: BlogPost;

  quillConfig = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],

      [ {header: 1}, {header: 2}],
      ['link']
    ]
  };

  constructor(private readTimeEstimator: ReadingTimeEstimatorService) { }

  ngOnInit() {
    this.BlogEntry.date = new Date(Date.now());
  }

  estimateReadTime() {
    this.BlogEntry.estimatedReadTime = this.readTimeEstimator.estimateReadingTime(htmlStripper(this.BlogEntry.content));
  }
}
