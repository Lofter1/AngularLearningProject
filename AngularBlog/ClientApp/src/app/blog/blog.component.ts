import { Component, OnInit } from '@angular/core';
import {BlogPostRepositoryService} from '../Repositories/blog-post-repository.service';
import {BlogPost} from '../Models/BlogPost';
import { Permission } from '../Models/Permission';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  private permission = Permission;
  posts: BlogPost[];

  constructor(private blogPostRepository: BlogPostRepositoryService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts(): void {
    this.blogPostRepository.getAll().subscribe(posts => this.posts = posts);
  }

}
