import {Component, Input, OnInit} from '@angular/core';
import {BlogPost} from '../../Models/BlogPost';
import {ActivatedRoute} from '@angular/router';
import {BlogPostRepositoryService} from '../../Repositories/blog-post-repository.service';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogpostComponent implements OnInit {
  @Input() post: BlogPost;
  estimatedTimeToRead: string;

  constructor(
    private route: ActivatedRoute,
    private blogpostRepository: BlogPostRepositoryService,
  ) { }

  ngOnInit() {
    if (this.post === undefined) {
      this.getPost();
    }
  }

  getTimeToRead(): string {
    if (this.post.estimatedReadTime < 0.7) {
     return 'less than a minute';
    } else if (Math.round(this.post.estimatedReadTime) === 1) {
      return '1 minute';
    } else {
      return Math.round(this.post.estimatedReadTime) + ' minutes';
    }
  }

  private getPost(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.blogpostRepository.getPost(id).subscribe(post => this.post = post);
  }

}
