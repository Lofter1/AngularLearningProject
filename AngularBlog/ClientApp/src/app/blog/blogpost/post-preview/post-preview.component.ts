import {Component, Input, OnInit} from '@angular/core';
import {BlogPost} from '../../../Models/BlogPost';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.css']
})
export class PostPreviewComponent implements OnInit {
  @Input() blogPost: BlogPost;

  constructor() { }

  ngOnInit() {
  }

}
