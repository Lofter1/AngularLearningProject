import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReadingTimeEstimatorService {
  averageWordsPerMinute = 225;

  constructor() { }

  estimateReadingTime(text: string): number {
    const matches = text.match(/[\w\d’'-]+/gi);
    const wordCount = matches ? matches.length : 0;
    console.log(wordCount);
    return wordCount / this.averageWordsPerMinute;
  }
}
