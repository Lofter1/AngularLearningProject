import { Component, OnInit } from '@angular/core';
import {BlogPost} from '../Models/BlogPost';
import {BlogPostRepositoryService} from '../Repositories/blog-post-repository.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  recentPosts: BlogPost[];

  constructor(private postRepository: BlogPostRepositoryService) { }

  ngOnInit() {
    this.getMostRecentPosts();
  }

  getMostRecentPosts(): void {
    this.postRepository.getAll()
      .subscribe(posts => this.recentPosts = posts.sort((a, b) => b.id - a.id).slice(0, 5));
  }

}
