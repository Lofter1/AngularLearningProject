import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class OnlyAdminsGuardService implements CanActivate {

  constructor(private userService: UserService) { }

  canActivate(): boolean {
    if (this.userService.currentUser.role === 'ADMIN') {
      console.log('is admin');
      return true;
    } else {
      return false;
    }
  }
}
