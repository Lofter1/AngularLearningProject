import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {BlogPost} from './Models/BlogPost';
import {Permission} from './Models/Permission';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements  InMemoryDbService {

  constructor() { }

  createDb() {
    const posts = [
      {
        id: 1,
        title: 'Bob',
        content: 'I really believe that if you practice enough you could paint the ' +
          '\'Mona Lisa\' with a two-inch brush. Nice little fluffy clouds laying around ' +
          'in the sky being lazy.\n',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      },
      {
        id: 2,
        title: 'Ross',
        content: 'Son of a gun. Of course he\'s a happy little stone, ' +
          'cause we don\'t have any other kind.',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      },
      {
        id: 3,
        title: 'is',
        content: 'We\'re trying to teach you a technique here and how to use it. ' +
          'Let\'s put some happy little clouds in our world.',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      },
      {
        id: 4,
        title: 'awesome',
        content: 'I think there\'s an artist hidden in the bottom of ' +
          'every single one of us. There\'s nothing wrong with having a tree as a friend.\n',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      },
      {
        id: 5,
        title: 'and',
        content: 'You don\'t want to kill all your dark areas they are very important. ' +
          'That\'s why I paint - because I can create the kind of' +
          ' world I want - and I can make this world as happy as I want it.',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      },
      {
        id: 6,
        title: 'handsome',
        content: 'You can create beautiful things - but you have to see them in your mind first.',
        date: new Date(Date.now()),
        estimatedReadTime: 0.5
      }
    ];

    const users = [
      {
        id: 1,
        firstName: 'ad',
        lastName: 'min',
        username: 'admin',
        password: 'password',
        role: 'ADMIN',
        permissions: [Permission.addBlogEntry]
      },
      {
        id: 2,
        firstName: 'justin',
        lastName: 'juers',
        username: 'jusjue',
        password: 'password',
        role: 'ADMIN',
        permissions: [Permission.addBlogEntry]
      },
      {
        id: 3,
        firstName: 'Someone',
        lastName: 'not important',
        username: 'someone',
        password: 'password'
      }
    ];

    return { posts, users };
  }

  genId(posts: BlogPost[]): number {
    return posts.length > 0 ? Math.max(...posts.map(hero => hero.id)) + 1 : 1;
  }
}
