import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private loginService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  login(username: string, password: string) {
    this.loginService.login(username, password).then(success => {
      if (success) {
        this.router.navigateByUrl('');
      }
    });
  }
}
