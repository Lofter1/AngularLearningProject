import { Component, OnInit } from '@angular/core';
import {User} from '../../Models/User';
import {UserRepositoryService} from '../../Repositories/user-repository.service';
import {ToastService} from '../../Common/toast/toast.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private newUser: User;
  passwordConfirm: string;

  constructor(private users: UserRepositoryService,
              private toasts: ToastService,
              private router: Router) { }

  ngOnInit() {
    this.newUser = new User();
  }

  validate(): boolean {
    return this.newUser.password === this.passwordConfirm;
  }

  register(): void {
    if (this.validate()) {
      this.users.add(this.newUser);
      this.router.navigate(['/dashboard']);
    } else {
      this.toasts.show('Registering failed',
        'Please check if all information are correct');
    }
  }

}
