import { Injectable } from '@angular/core';
import {User} from '../Models/User';
import {UserRepositoryService} from '../Repositories/user-repository.service';
import {ToastService} from '../Common/toast/toast.service';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: User;

  constructor(private userRepository: UserRepositoryService,
              private toastService: ToastService,
              private roleService: NgxRolesService,
              private permissionService: NgxPermissionsService) { }

  async login(username: string, password: string): Promise<boolean> {
    let success = false;

    await this.userRepository.getAll().toPromise()
      .then(users => {
        const user = users.find(u => u.username === username);
        if (user.password === password) {
          this.loginAs(user);
          success = true;
        } else {
          this.toastService.show('Login failed', 'Please check Username and Password', 10000);
        }
      });

    return success;
  }

  signOut(): void {
    this.currentUser = null;
    this.roleService.flushRoles();
    this.permissionService.flushPermissions();
    this.toastService.show('Sign out', 'You signed out', 5000);
  }

  private loginAs(user: User): void {
    this.currentUser = user;
    this.permissionService.addPermission(this.currentUser.permissions);
    this.roleService.addRole(this.currentUser.role, this.currentUser.permissions);
    this.toastService
      .show(`Signed in as ${this.currentUser.username}`, `Hello ${this.currentUser.firstName}`, 5000);
  }
}
